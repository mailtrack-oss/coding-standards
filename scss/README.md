# SCSS Coding Standard

We use stylelint to find problems and follow a coding standard in our SCSS files.

Our CS is based in Stylelint Standard Rules. You can check out these rules here: https://github.com/stylelint/stylelint-config-standard

We do not follow the Airbnb CSS Style Guide, but it is a good reference to read, and has lots of rules in common
with stylelint standard rules. Check out the guide here: https://github.com/airbnb/css#css

## How to config CS in a new project

* Install required node modules: `npm install --save-dev stylelint stylelint-config-standard stylelint-order` 
* Add this line to the devDependencies property of the package.json file:
     ```
     "stylelint-config-mt-cs": "https://gitlab.com/mailtrack-oss/coding-standards.git",
     ```
* run `npm install`
* create a file called `.stylelintrc` in the project's root with this content:
    ```json
    {
        "extends": [
            "stylelint-config-mt-cs/scss"
        ]
    }
    ```
  
## How to use a specific branch or tag of the CS

By default, NPM will install the master branch of this project. If you need to use another branch or tag append `#branch_name` or `#tag_name` to the git url in the package.json. For example:
```
"stylelint-config-mt-cs": "https://gitlab.com/mailtrack-oss/coding-standards.git#1.0.0",
```

## How to test changes in local

If you make some changes, and you want to test them before pushing the code, you can change the dependency url to the local path. Por example:
```
"stylelint-config-mt-cs": "../coding-standards"
```

## Configure Stylelint in your IDE

* PHP Storm: https://www.jetbrains.com/help/phpstorm/using-stylelint-code-quality-tool.html
* VS Code: https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint