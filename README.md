# Mailtrack Coding Standards

This project contains coding standards documentation and config files for our projects

* [Javascript](./javascript)
* [Typescript](./typescript)
* [SCSS](./scss)

## Semantic Versioning

This project is semantic versioned, please create a new version tag if you make changes.

## Common Rules
### Development Language
English is our main development language. That means that everything in the code
should be written in English: classes, entities, functions, methods, etc. If there
is a new object needed, think about the best english term to use and check it with some mate.

It does not matter if Business handles terms in Spanish, we should be capable of understanding
and translating it into English and viceversa.

## Related resources

* [Extension architecture](https://gitlab.com/mailtrack/extension/-/blob/master/app/docs/architecture.md)
* [Backend Coding Standard](https://gitlab.com/mailtrack/backend/-/blob/master/docs/tech/coding-standard.md)
